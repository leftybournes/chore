use structopt::StructOpt;

use crate::core::Core;

#[derive(StructOpt)]
enum Command {
    Add {
        #[structopt(parse(from_os_str))]
        file: std::path::PathBuf,
    },
    Remove {
        #[structopt(parse(from_os_str))]
        file: std::path::PathBuf,
    }
}

#[derive(StructOpt)]
pub struct Cli {
    #[structopt(subcommand)]
    command: Command,
}

impl Cli {
    pub fn run() -> Result<bool, String> {
        let args = Cli::from_args();

        match args.command {
            Command::Add{file} => {
                return Core::add(&file);
            },
            Command::Remove{file} => Core::remove(&file)
        }

        Ok(true)
    }
}
