use std::fs::copy;
use std::path::Path;

use crate::config::Config;

pub struct Core;

impl Core {
    const QUALIFIER: &'static str = "com";
    const ORGANIZATION: &'static str = "kylobytes";
    const APPLICATION: &'static str = "chore";


    pub fn add(file: &Path) -> Result<bool, String> {
        let is_file_present = file.exists();

        if !is_file_present {
            return Err(
                format!(
                    "Failed to add {}: no such file or directory",
                    file.display()
                )
            );
        }

        let config = Config::new(
            &Self::QUALIFIER,
            &Self::ORGANIZATION,
            &Self::APPLICATION
        )?;

        let file_name = file.file_name()
            .and_then(|file_name| -> Option<&str> {
                file_name.to_str()
            })
            .unwrap_or("None");


        let destination_str = format!("{}/chore/{}", config.data_dir, file_name);
        println!("{}", destination_str);
        let destination = Path::new(
            &destination_str
        );

        let copy_result = copy(file, destination);

        match copy_result {
            Ok(size) => {
                println!(
                    "Successfuly add {}. {} bytes copied",
                    file.display(),
                    size
                );
            },
            Err(e) => {
                return Err(format!("Failed to add {}. {}", file.display(),  e));
            }
        }
        
        Ok(true)
    }

    pub fn remove(file: &Path) {
        if file.exists() {
            println!("Remove {:?}?", file);
        } else {
            println!("Can't remove {:?}. It does not exist", file);
        }
    }

}
