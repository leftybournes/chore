use std::path::Path;

const XDG_DATA_HOME: &str = "XDG_DATA_HOME";
// const XDG_CONFIG_HOME: &str = "XDG_CONFIG_HOME";
const USER_HOME: &str = "HOME";
const DATA_DIR: &str = ".local/share";
const CONFIG_FILE: &str = ".config/chore/chore.toml";

pub struct Config {
    pub user_home: String,
    pub data_dir: String,
    pub config_file: Option<String>,
    pub sync_dir: String
}

impl Config {
    pub fn new() -> Result<Config, String> {

        let data_dir_result = std::env::var(XDG_DATA_HOME);
        let user_home_result = std::env::var(USER_HOME);

        let data_dir: String;
        let user_home: String;

        match user_home_result {
            Ok(val) => {
                user_home = String::from(val);
            },
            Err(_) => { panic!("HOME not present") }
        }

        let config_file = Self::initialize_config_file(&user_home);

        match data_dir_result {
            Ok(val) => {
                data_dir = String::from(val);
            },
            Err(_) => {
                // In the case that XDG_DATA_DIR isn't present, set data_dir to
                // $HOME/.local/share. This is because some linux distributions
                // don't have it set, especially non-gnome/non-gtk
                // environments.
                data_dir = format!("{}/{}", user_home, DATA_DIR);
            }
        }

        let sync_dir = Self::initialize_sync_dir(&data_dir);

        Ok(Config { data_dir, user_home, config_file, sync_dir })
    }

    // TODO: Add checking for XDG_CONFIG_HOME. If present, use it. Else,
    // fall back to the usual .config in user's HOME
    fn initialize_config_file(user_home: &String) -> Option<String> {
        // let config_home_result = std::env::var(XDG_CONFIG_HOME);

        let config_file_str = format!("{}/{}", user_home, CONFIG_FILE);
        let config_file = Path::new(&config_file_str);

        if config_file.exists() {
            return Some(config_file_str);
        }

        None
    }

    // TODO: Add config file checking and then checking if a the sync dir is
    // set. If it's set, use that path, if not, use a default value.
    fn initialize_sync_dir(data_dir: &String) -> String {
        return format!("{}/{}/chore", data_dir, DATA_DIR);
    }
}
