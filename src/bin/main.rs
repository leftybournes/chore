use chore::cli::Cli;

fn main() {
    let result = Cli::run();

    match result {
        Ok(true) => std::process::exit(0),
        Ok(false) => std::process::exit(1),
        Err(err) => {
            eprintln!("Error running chore: {}", err);
            std::process::exit(1)
        }
    }
}
