# Chore

Do your chores to keep your $HOME in order. In other words, manage your 
dotfiles. Inspired by [chezmoi](https://www.chezmoi.io) and 
[yadm](https://yadm.io).

## Why?

Why not? 

Seriously, though. I thought it would be a good exercise for me to learn
[rust](https://rust-lang.org). A simple project like a dotfiles manager would
be a great way to get my feet wet in the language.

On another note, I kind of liked the solutions provided by the previously
mentioned tools but not quite. There were aspects to each that I didn't like 
and were deal breakers to me.

With yadm, I would manage my dotfiles at the root level (not that root) of my
$HOME directory and it seemed like it could easily get messed up when not 
careful. The same goes for `git --bare`.

After trying chezmoi, it was almost perfect. Having used it for a while, I 
liked the way it did things. The problem was with how I'd have to edit dotfiles
in the data directory, sync that up with $HOME, and only then I'd be able to 
seeing the changes. As an emacs user, it would complain that it couldn't find 
the melpa packages installed on my machine.

I prefer to edit my configs directly in $HOME and then sync those changes to
the data directory, as chezmoi does.

The goal is to scratch a personal itch and make managing my personal dotfiles
stupidly simple.

## License

This project is licensed under the GNU General Public License version 3 only.
See [license](COPYING).
